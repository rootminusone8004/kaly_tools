# kaly_tools

![License: GPL-3.0-or-later](https://img.shields.io/badge/License-GPL--3.0--or--later-blue.svg)

It is just an attempt to mimic another project named [katoolin](https://github.com/LionSec/katoolin). It just contains the basic Kali Linux tools to do some basic pentesting stuffs. The script goes over the installation of basic tools so that you can don\'t have to type command again and again.

## important note

This tool is for educational purpose only. Besides, it only works in **Debian and its derivatives**. Therefore, Unstable Debian or Kali Linux is recommended.

## installation

See [INSTALL.org](INSTALL.org) for details.

## execution

Navigate to the repository and execute:

``` bash
./kaly_tools
```

Or if you want to manage repositories and install programs, then
execute:

``` bash
sudo ./kaly_tools
```

## License

kaly_tools - kali linux pentesting tools installer<br>
Copyright (C) 2021-present M.H. Rahman Kwoshik

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a [copy](LICENSE.txt) of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
