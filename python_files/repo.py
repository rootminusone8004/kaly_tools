import subprocess as s
import csv
from dialog import Dialog
import os
import time
import aptt

repoDatabase = []
with open("./csv_files/repo.csv", "r") as file:
    csvFile = csv.reader(file)
    for row in csvFile:
        repoDatabase.append(row)
file.close()


def addRepo(repoRow):
    sourceFile = "/etc/apt/sources.list.d/" + repoRow[0] + ".list"
    if os.path.exists(sourceFile):
        return

    if "http" in repoRow[1]:
        command = (
            "wget -qO- "
            + repoRow[1]
            + " | gpg --dearmor | tee /etc/apt/trusted.gpg.d/"
            + repoRow[0]
            + ".gpg > /dev/null"
        )
    else:
        command = "cp gpg_files/" + repoRow[1] + " /etc/apt/trusted.gpg.d/"
    os.system(command)
    command = (
        'echo "# '
        + repoRow[0]
        + " repo added by kaly_tools\ndeb [signed-by=/etc/apt/trusted.gpg.d/"
        + repoRow[0]
        + ".gpg arch=amd64] "
        + repoRow[2]
        + '" | tee /etc/apt/sources.list.d/'
        + repoRow[0]
        + ".list"
    )
    os.system(command)
    command = (
        'echo "Package: *\nPin: release o='
        + repoRow[3]
        + "\nPin-Priority: "
        + repoRow[4]
        + '" | tee /etc/apt/preferences.d/'
        + repoRow[0]
        + ".pref"
    )
    os.system(command)


def remove(fileName):
    preferenceFile = "/etc/apt/preferences.d/" + fileName + ".pref"
    sourceFile = "/etc/apt/sources.list.d/" + fileName + ".list"
    gpgFile = "/etc/apt/trusted.gpg.d/" + fileName + ".gpg"

    if os.path.exists(preferenceFile):
        os.remove(preferenceFile)
    else:
        print("preference file not found!")
        time.sleep(1)
    if os.path.exists(sourceFile):
        os.remove(sourceFile)
    else:
        print("source file not found!")
        time.sleep(1)
    if os.path.exists(gpgFile):
        os.remove(gpgFile)
    else:
        print("gpg file not found!")
        time.sleep(1)
    confirmMsg = (
        "\033[1;31m\n" + fileName + " repositories have been deleted !\n\033[1;m"
    )
    print(confirmMsg)
    time.sleep(1)


def driver():
    while True:
        aptt.checkPackage("wget")
        aptt.checkPackage("gnupg")

        d = Dialog(dialog="dialog")
        button_names = {
            d.OK: "OK",
            d.CANCEL: "Cancel",
            d.HELP: "Help",
            d.EXTRA: "Extra",
        }

        code, tag = d.menu(
            "additional repositories",
            choices=[
                ("1", "add repositories and update"),
                ("2", "update the system"),
                ("3", "remove repositories"),
            ],
        )

        if code == d.CANCEL:
            return
        m = int(tag)

        choice2 = []
        repoLength = len(repoDatabase)
        for p in range(repoLength):
            choice2.append((str(p + 1), "add " + repoDatabase[p][0] + " repositories"))
        choice2.append((str(repoLength + 1), "add all repositories"))

        if m == 1:
            code1, tag1 = d.menu("additional repositories", choices=choice2)
            if code1 == d.CANCEL:
                return
            m1 = int(tag1)

            if m1 <= repoLength:
                addRepo(repoDatabase[m1 - 1])
            elif m1 == repoLength + 1:
                for m3 in range(repoLength):
                    addRepo(repoDatabase[m3])
            else:
                pass
            os.system("exit")
        elif m == 2:
            os.system("clear")
            s.call(["apt", "update"])
        elif m == 3:
            choice3 = []
            for p in range(repoLength):
                choice3.append(
                    (str(p + 1), "remove " + repoDatabase[p][0] + " repositories")
                )
            choice3.append((str(repoLength + 1), "remove all repositories"))
            code2, tag2 = d.menu("additional repositories", choices=choice3)

            if code2 == d.CANCEL:
                return
            m2 = int(tag2)
            if m2 <= repoLength:
                remove(repoDatabase[m2 - 1][0])
            elif m2 == repoLength + 1:
                for m3 in range(repoLength):
                    remove(repoDatabase[m3][0])
            else:
                pass
        else:
            pass
    return
