#!/usr/bin/python3

import subprocess as s
import os
import csv

try:
    from dialog import Dialog
except:
    print("Please install the package called python3-dialog. Quitting...")
    exit()

import gitt
import repo
import aptt
import mold

d = Dialog(dialog="dialog")

button_names = {d.OK: "OK", d.CANCEL: "Cancel", d.HELP: "Help", d.EXTRA: "Extra"}

label_csv_of_choice = os.listdir("csv_files/software_list/")
csv_of_choice = []
for k in range(len(label_csv_of_choice)):
    csv_of_choice.append((str(k + 1), label_csv_of_choice[k]))


def wrapper():
    k = 99
    if os.geteuid() == 0:
        d.msgbox(
            "welcome to KalyTools. This script will help you install your desired packages."
        )

        code1, tag1 = d.menu("Select appropriate csv file", choices=csv_of_choice)
        if code1 == d.CANCEL:
            d.msgbox("Thanks for using me.")
            quit()

        k = int(tag1)
        csvFile2 = label_csv_of_choice[k - 1]

        database = []
        path_csv = "csv_files/software_list/" + csvFile2
        with open(path_csv, "r") as file:
            csvFile = csv.reader(file)
            for row in csvFile:
                database.append(row)
        file.close()

        label = []
        for k in database:
            label.append(k[0])
        choice = []
        for k in range(len(label)):
            choice.append((str(k + 1), label[k]))

        while k != 0:

            code, tag = d.menu("Select the tool(s) you want to install", choices=choice)

            if code == d.CANCEL:
                d.msgbox("Thanks for using me.")
                break

            k = int(tag)

            if k == 1:
                repo.driver()
            elif k > 1 and k <= len(database):
                mold.driver(label[k - 1], database[k - 1])
            else:
                break
    else:
        d.msgbox(
            "welcome to KalyTools. Since you ran it without sudo, packages from repositories can't be installed."
        )
        while k != 0:
            code, tag = d.menu(
                "What kind of tool(s) you want to install",
                choices=[("1", "git packages")],
            )

            if code == d.CANCEL:
                d.msgbox("Thanks for using me.")
                break

            k = int(tag)
            if k == 1:
                gitt.driver()

    os.system("clear")


# try: wrapper()
# except: exit()

wrapper()
