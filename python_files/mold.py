import subprocess as s
from dialog import Dialog
import os
import apt
import time


def driver(bannr, tools):

    d = Dialog(dialog="dialog")
    button_names = {d.OK: "OK", d.CANCEL: "Cancel", d.HELP: "Help", d.EXTRA: "Extra"}

    cache = apt.Cache()
    while True:
        p = ["apt", "install"]
        tool = tools + ["all"]
        choice = []
        length = len(tool)
        for k in range(1, length):
            choice.append((str(k), tool[k]))

        code, tag = d.menu(bannr, choices=choice)
        if code == d.CANCEL:
            return
        m = int(tag)
        if m < length - 1:
            if not tool[m] in cache:
                d.msgbox("Add appropriate repositories, update and then try again")
                return
            else:
                if cache[tool[m]].is_installed:
                    d.msgbox("The selected package is already installed")
                    return
            p += tool[m : m + 1]
            os.system("clear")
            s.call(p)
        elif m == length - 1:
            os.system("clear")
            for j in range(1, length):
                s.call(p + [tool[j]])
        else:
            pass
    return
