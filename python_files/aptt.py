import subprocess as s
from dialog import Dialog
import apt
import os


def checkPackage(packageName):
    d = Dialog(dialog="dialog")
    button_names = {d.OK: "OK"}
    cache = apt.Cache()
    if packageName in cache:
        if cache[packageName].is_installed == False:
            s.call(["apt", "install", packageName])
    else:
        d.msgbox(
            "Your distribution doesn't ship package called "
            + packageName
            + ".\nPlease make sure it is installed first."
        )
        os.system("clear")
        exit()
