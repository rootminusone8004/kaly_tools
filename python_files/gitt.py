import subprocess as s
from dialog import Dialog
import os
import csv
import apt


def driver():
    d = Dialog(dialog="dialog")
    cache = apt.Cache()
    if cache["git"].is_installed == False:
        d.msgbox("You don't have git installed. Make sure you have installed it first")
        os.system("clear")
        quit()

    gitRepo = []
    with open("./csv_files/git.csv", "r") as file:
        csvFile = csv.reader(file)
        for row in csvFile:
            gitRepo.append(row)
    file.close()

    button_names = {d.OK: "OK", d.CANCEL: "Cancel", d.HELP: "Help", d.EXTRA: "Extra"}

    gitFolder = "git_files"
    if not os.path.isdir(gitFolder):
        os.makedirs(gitFolder)
    os.chdir(gitFolder)
    p = ["git", "clone"]

    choice2 = []
    for z in range(len(gitRepo)):
        choice2.append((str(z + 1), gitRepo[z][0]))
    code, tag = d.menu("Select your git package", choices=choice2)

    if code == d.CANCEL:
        return

    m = int(tag)
    p += [gitRepo[m - 1][1]]
    os.system("clear")
    s.call(p)
    os.chdir("..")
    return
