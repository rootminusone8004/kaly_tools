* Installation

Installation is for *debian and its derivatives only*.

** download packages

In order to run this program, you need the following packages to be installed.

- python
#+begin_src bash
  sudo apt install python3
#+end_src
- git
#+begin_src bash
  sudo apt install git
#+end_src
Also, you need to be an user (not root) with sudo privileges.

** clone repository

Open your terminal and execute this:
#+begin_src bash
  git clone "https://codeberg.org/rootminusone8004/kaly_tools"
#+end_src
